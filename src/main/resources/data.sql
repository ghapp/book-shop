-- DO $$ DECLARE
--     table_name text;
-- BEGIN
--     FOR table_name IN (SELECT tablename FROM pg_tables WHERE schemaname = 'public')
--         LOOP
--             EXECUTE 'DROP TABLE IF EXISTS public.' || table_name || ' CASCADE';
--         END LOOP;
-- END $$;

insert into authors (id, photo, slug, name, description) values (1, 'http://dummyimage.com/50x69.png/cc0000/ffffff', 'betsey-lamputt', 'Betsey Lamputt', 'synergize killer schemas');
insert into authors (id, photo, slug, name, description) values (2, 'http://dummyimage.com/50x90.png/5fa2dd/ffffff', 'libbie-walliker', 'Libbie Walliker', 'morph bricks-and-clicks supply-chains');
insert into authors (id, photo, slug, name, description) values (3, 'http://dummyimage.com/50x90.png/cc0000/ffffff', 'rowland-cluett', 'Rowland Cluett', 'implement compelling relationships');
insert into authors (id, photo, slug, name, description) values (4, 'http://dummyimage.com/50x71.png/ff4444/ffffff', 'benedick-ferres', 'Benedick Ferres', 'benchmark end-to-end schemas');
insert into authors (id, photo, slug, name, description) values (5, 'http://dummyimage.com/50x85.png/cc0000/ffffff', 'daffi-boog', 'Daffi Boog', 'transition robust solutions');
insert into authors (id, photo, slug, name, description) values (6, 'http://dummyimage.com/50x80.png/5fa2dd/ffffff', 'mic-filkov', 'Mic Filkov', 'utilize e-business channels');
insert into authors (id, photo, slug, name, description) values (7, 'http://dummyimage.com/50x87.png/cc0000/ffffff', 'joletta-staynes', 'Joletta Staynes', 'repurpose bleeding-edge channels');
insert into authors (id, photo, slug, name, description) values (8, 'http://dummyimage.com/50x100.png/5fa2dd/ffffff', 'suki-tinkham', 'Suki Tinkham', 'repurpose end-to-end models');
insert into authors (id, photo, slug, name, description) values (9, 'http://dummyimage.com/50x72.png/dddddd/000000', 'leandra-peddel', 'Leandra Peddel', 'envisioneer mission-critical metrics');
insert into authors (id, photo, slug, name, description) values (10, 'http://dummyimage.com/50x70.png/ff4444/ffffff', 'seka-bonn', 'Seka Bonn', 'visualize end-to-end methodologies');
insert into authors (id, photo, slug, name, description) values (11, 'http://dummyimage.com/50x94.png/ff4444/ffffff', 'georgie-st-john', 'Georgie St. John', 'expedite enterprise methodologies');
insert into authors (id, photo, slug, name, description) values (12, 'http://dummyimage.com/50x95.png/cc0000/ffffff', 'jourdain-willoughby', 'Jourdain Willoughby', 'expedite web-enabled functionalities');
insert into authors (id, photo, slug, name, description) values (13, 'http://dummyimage.com/50x72.png/cc0000/ffffff', 'claudianus-idell', 'Claudianus Idell', 'scale integrated communities');
insert into authors (id, photo, slug, name, description) values (14, 'http://dummyimage.com/50x71.png/5fa2dd/ffffff', 'salim-mcduffy', 'Salim McDuffy', 'enable B2B platforms');
insert into authors (id, photo, slug, name, description) values (15, 'http://dummyimage.com/50x62.png/dddddd/000000', 'anthea-elgram', 'Anthea Elgram', 'transition enterprise vortals');
insert into authors (id, photo, slug, name, description) values (16, 'http://dummyimage.com/50x61.png/ff4444/ffffff', 'simeon-holbarrow', 'Simeon Holbarrow', 'seize cross-platform convergence');
insert into authors (id, photo, slug, name, description) values (17, 'http://dummyimage.com/50x74.png/cc0000/ffffff', 'deana-ringham', 'Deana Ringham', 'scale compelling supply-chains');
insert into authors (id, photo, slug, name, description) values (18, 'http://dummyimage.com/50x88.png/dddddd/000000', 'johnathon-arnould', 'Johnathon Arnould', 'architect holistic bandwidth');
insert into authors (id, photo, slug, name, description) values (19, 'http://dummyimage.com/50x55.png/ff4444/ffffff', 'maximilien-wannan', 'Maximilien Wannan', 'incubate cross-media paradigms');
insert into authors (id, photo, slug, name, description) values (20, 'http://dummyimage.com/50x77.png/5fa2dd/ffffff', 'addie-turbard', 'Addie Turbard', 'incentivize seamless infrastructures');
insert into authors (id, photo, slug, name, description) values (21, 'http://dummyimage.com/50x67.png/5fa2dd/ffffff', 'gothart-fike', 'Gothart Fike', 'cultivate web-enabled content');
insert into authors (id, photo, slug, name, description) values (22, 'http://dummyimage.com/50x62.png/5fa2dd/ffffff', 'bertie-buddell', 'Bertie Buddell', 'engage ubiquitous interfaces');
insert into authors (id, photo, slug, name, description) values (23, 'http://dummyimage.com/50x90.png/5fa2dd/ffffff', 'henrik-schurig', 'Henrik Schurig', 'architect integrated communities');
insert into authors (id, photo, slug, name, description) values (24, 'http://dummyimage.com/50x63.png/cc0000/ffffff', 'raphaela-hovenden', 'Raphaela Hovenden', 'unleash leading-edge action-items');
insert into authors (id, photo, slug, name, description) values (25, 'http://dummyimage.com/50x74.png/dddddd/000000', 'frans-gilhooly', 'Frans Gilhooly', 'transition strategic partnerships');
insert into authors (id, photo, slug, name, description) values (26, 'http://dummyimage.com/50x60.png/cc0000/ffffff', 'dale-ranger', 'Dale Ranger', 'repurpose interactive technologies');
INSERT INTO authors (id, photo, slug, name, description) VALUES (27, 'http://dummyimage.com/50x71.png/5fa2dd/ffffff', 'anna-maria-leetham', 'Anna-maria Leetham', 'mesh out-of-the-box technologies');
INSERT INTO authors (id, photo, slug, name, description) VALUES (28, 'http://dummyimage.com/50x78.png/dddddd/000000', 'pet-calverley', 'Pet Calverley', 'whiteboard back-end mindshare');
INSERT INTO authors (id, photo, slug, name, description) VALUES (29, 'http://dummyimage.com/50x84.png/ff4444/ffffff', 'normy-kelberer', 'Normy Kelberer', 'expedite world-class web services');
INSERT INTO authors (id, photo, slug, name, description) VALUES (30, 'http://dummyimage.com/50x55.png/dddddd/000000', 'pauline-chooter', 'Pauline Chooter', 'orchestrate B2C mindshare');
INSERT INTO authors (id, photo, slug, name, description) VALUES (31, 'http://dummyimage.com/50x95.png/cc0000/ffffff', 'ardene-oday', 'Ardene O''Day', 'extend global e-business');
INSERT INTO authors (id, photo, slug, name, description) VALUES (32, 'http://dummyimage.com/50x79.png/dddddd/000000', 'toiboid-sandy', 'Toiboid Sandy', 'harness user-centric mindshare');
INSERT INTO authors (id, photo, slug, name, description) VALUES (33, 'http://dummyimage.com/50x81.png/ff4444/ffffff', 'annetta-swetman', 'Annetta Swetman', 'extend one-to-one architectures');
INSERT INTO authors (id, photo, slug, name, description) VALUES (34, 'http://dummyimage.com/50x63.png/cc0000/ffffff', 'tomlin-hansberry', 'Tomlin Hansberry', 'seize turn-key bandwidth');
INSERT INTO authors (id, photo, slug, name, description) VALUES (35, 'http://dummyimage.com/50x80.png/cc0000/ffffff', 'sabina-tizzard', 'Sabina Tizzard', 'incubate granular e-markets');
INSERT INTO authors (id, photo, slug, name, description) VALUES (36, 'http://dummyimage.com/50x85.png/ff4444/ffffff', LOWER(REPLACE('Eleanore Melby', ' ', '-')), 'Eleanore Melby', 'architect user-centric models');
INSERT INTO authors (id, photo, slug, name, description) VALUES (37, 'http://dummyimage.com/50x56.png/5fa2dd/ffffff', LOWER(REPLACE('Keith Testro', ' ', '-')), 'Keith Testro', 'leverage leading-edge convergence');
INSERT INTO authors (id, photo, slug, name, description) VALUES (38, 'http://dummyimage.com/50x99.png/dddddd/000000', LOWER(REPLACE('Albina Stampe', ' ', '-')), 'Albina Stampe', 'embrace synergistic supply-chains');
INSERT INTO authors (id, photo, slug, name, description) VALUES (39, 'http://dummyimage.com/50x63.png/ff4444/ffffff', LOWER(REPLACE('Tori Frushard', ' ', '-')), 'Tori Frushard', 'revolutionize next-generation e-services');
INSERT INTO authors (id, photo, slug, name, description) VALUES (40, 'http://dummyimage.com/50x51.png/5fa2dd/ffffff', LOWER(REPLACE('Dalt Plenderleith', ' ', '-')), 'Dalt Plenderleith', 'repurpose impactful metrics');
INSERT INTO authors (id, photo, slug, name, description) VALUES (41, 'http://dummyimage.com/50x78.png/dddddd/000000', LOWER(REPLACE('Rosita Spira', ' ', '-')), 'Rosita Spira', 'morph proactive paradigms');
INSERT INTO authors (id, photo, slug, name, description) VALUES (42, 'http://dummyimage.com/50x55.png/cc0000/ffffff', LOWER(REPLACE('Ramonda Pedlow', ' ', '-')), 'Ramonda Pedlow', 'optimize impactful solutions');
INSERT INTO authors (id, photo, slug, name, description) VALUES (43, 'http://dummyimage.com/50x69.png/ff4444/ffffff', LOWER(REPLACE('Davidson Broadway', ' ', '-')), 'Davidson Broadway', 'scale seamless communities');
insert into authors (id, photo, slug, name, description) values (44, 'http://dummyimage.com/50x78.png/dddddd/000000', 'sigmund-cunniff', 'Sigmund Cunniff', 'unleash dot-com bandwidth');
insert into authors (id, photo, slug, name, description) values (45, 'http://dummyimage.com/50x69.png/ff4444/ffffff', 'putnem-keetley', 'Putnem Keetley', 'expedite cross-platform experiences');
insert into authors (id, photo, slug, name, description) values (46, 'http://dummyimage.com/50x51.png/ff4444/ffffff', 'kristine-shadwick', 'Kristine Shadwick', 'e-enable B2B initiatives');
insert into authors (id, photo, slug, name, description) values (47, 'http://dummyimage.com/50x63.png/cc0000/ffffff', 'deloria-lowles', 'Deloria Lowles', 'incentivize dot-com networks');
insert into authors (id, photo, slug, name, description) values (48, 'http://dummyimage.com/50x63.png/5fa2dd/ffffff', 'stephi-baldery', 'Stephi Baldery', 'cultivate sticky e-business');
insert into authors (id, photo, slug, name, description) values (49, 'http://dummyimage.com/50x98.png/cc0000/ffffff', 'johann-bernardeau', 'Johann Bernardeau', 'innovate transparent functionalities');
insert into authors (id, photo, slug, name, description) values (50, 'http://dummyimage.com/50x98.png/ff4444/ffffff', 'annie-chellam', 'Annie Chellam', 'cultivate out-of-the-box deliverables');

INSERT INTO books (id, pub_date, is_bestseller, slug, title, image, author_id, description, price, discount)
VALUES
    (1, '2022-06-18', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Heldorado', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Heldorado', 'http://dummyimage.com/119x100.png/ff4444/ffffff', 23, 'id consequat in consequat ut nulla sed accumsan felis ut', 1221, 15),
    (2, '2023-04-15', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Oscar', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Oscar', 'http://dummyimage.com/239x100.png/cc0000/ffffff', 39, 'sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl', 4943, 18),
    (3, '2022-04-23', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Scooby-Doo! and the Witch''s Ghost', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Scooby-Doo! and the Witch''s Ghost', 'http://dummyimage.com/170x100.png/5fa2dd/ffffff', 4, 'tempus semper est quam pharetra magna ac consequat metus sapien', 6329, 15),
    (4, '2023-02-20', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Appleseed (Appurushîdo)', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Appleseed (Appurushîdo)', 'http://dummyimage.com/198x100.png/5fa2dd/ffffff', 1, 'viverra eget congue eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien', 8513, 2),
    (5, '2022-08-09', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Substitute, The', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Substitute, The', 'http://dummyimage.com/109x100.png/5fa2dd/ffffff', 36, 'augue aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse', 916, 13);
INSERT INTO books (id, pub_date, is_bestseller, slug, title, image, author_id, description, price, discount)
VALUES
    (6, '2022-08-02', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Red Planet', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Red Planet', 'http://dummyimage.com/124x100.png/dddddd/000000', 50, 'lobortis ligula sit amet eleifend pede libero quis orci nullam molestie nibh', 2589, 6),
    (7, '2022-09-19', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('After Life (Wandafuru raifu)', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'After Life (Wandafuru raifu)', 'http://dummyimage.com/164x100.png/dddddd/000000', 23, 'erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo', 197, 8),
    (8, '2023-01-28', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Ransom', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Ransom', 'http://dummyimage.com/138x100.png/cc0000/ffffff', 39, 'donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit', 9792, 18),
    (9, '2022-06-15', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Naughty Room, The', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Naughty Room, The', 'http://dummyimage.com/158x100.png/cc0000/ffffff', 21, 'at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus vel pede', 4124, 8),
    (10, '2022-12-30', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('All Dogs Go to Heaven', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'All Dogs Go to Heaven', 'http://dummyimage.com/233x100.png/dddddd/000000', 14, 'in porttitor pede justo eu massa donec dapibus duis at velit eu est congue elementum', 1813, 8);
INSERT INTO books (id, pub_date, is_bestseller, slug, title, image, author_id, description, price, discount)
VALUES
    (11, '2022-11-25', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Louise-Michel', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Louise-Michel', 'http://dummyimage.com/152x100.png/5fa2dd/ffffff', 41, 'curabitur at ipsum ac tellus semper interdum mauris ullamcorper purus sit amet nulla quisque arcu libero rutrum ac lobortis', 1490, 11),
    (12, '2023-02-01', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Ingmar Bergman Makes a Movie (Ingmar Bergman gör en film)', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Ingmar Bergman Makes a Movie (Ingmar Bergman gör en film)', 'http://dummyimage.com/114x100.png/cc0000/ffffff', 16, 'convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor', 7357, 7),
    (13, '2022-05-21', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Presumed Guilty (Presunto culpable)', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Presumed Guilty (Presunto culpable)', 'http://dummyimage.com/173x100.png/dddddd/000000', 15, 'enim in tempor turpis nec euismod scelerisque quam turpis adipiscing', 2802, 17),
    (14, '2023-01-20', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Antares', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Antares', 'http://dummyimage.com/100x100.png/5fa2dd/ffffff', 20, 'cras mi pede malesuada in imperdiet et commodo vulputate justo', 3886, 13),
    (15, '2023-02-09', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('In the Loop', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'In the Loop', 'http://dummyimage.com/233x100.png/5fa2dd/ffffff', 12, 'ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer', 5153, 20);
INSERT INTO books (id, pub_date, is_bestseller, slug, title, image, author_id, description, price, discount)
VALUES
    (16, '2022-06-19', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Tower Heist', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Tower Heist', 'http://dummyimage.com/196x100.png/cc0000/ffffff', 45, 'diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam quis turpis eget', 8274, 17),
    (17, '2022-05-04', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Jönssonligan dyker upp igen', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Jönssonligan dyker upp igen', 'http://dummyimage.com/243x100.png/cc0000/ffffff', 44, 'molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget', 6171, 3),
    (18, '2022-05-14', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Landscape Suicide', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Landscape Suicide', 'http://dummyimage.com/128x100.png/cc0000/ffffff', 19, 'massa quis augue luctus tincidunt nulla mollis molestie lorem quisque ut erat curabitur gravida nisi at nibh in', 4644, 20),
    (19, '2023-04-06', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Four Bags Full', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Four Bags Full', 'http://dummyimage.com/159x100.png/5fa2dd/ffffff', 42, 'in eleifend quam a odio in hac habitasse platea dictumst maecenas', 849, 7),
    (20, '2023-02-28', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Bad Girl', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Bad Girl', 'http://dummyimage.com/148x100.png/5fa2dd/ffffff', 34, 'ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor', 997, 8);
INSERT INTO books (id, pub_date, is_bestseller, slug, title, image, author_id, description, price, discount)
VALUES
    (21, '2022-12-22', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Basic Instinct', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Basic Instinct', 'http://dummyimage.com/207x100.png/cc0000/ffffff', 50, 'rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id consequat', 7845, 13),
    (22, '2022-10-03', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Monster in a Box', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Monster in a Box', 'http://dummyimage.com/221x100.png/cc0000/ffffff', 44, 'tortor quis turpis sed ante vivamus tortor duis mattis egestas metus aenean fermentum donec', 5630, 4),
    (23, '2023-02-15', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Daytrippers, The', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Daytrippers, The', 'http://dummyimage.com/217x100.png/dddddd/000000', 18, 'phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin', 2356, 15),
    (24, '2022-08-12', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Otello', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Otello', 'http://dummyimage.com/207x100.png/cc0000/ffffff', 21, 'sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel', 3660, 6),
    (25, '2023-01-28', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Tall Blond Man with One Black Shoe, The (Le grand blond avec une chaussure noire)', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Tall Blond Man with One Black Shoe, The (Le grand blond avec une chaussure noire)', 'http://dummyimage.com/227x100.png/cc0000/ffffff', 49, 'vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus orci luctus et ultrices', 2040, 11);
INSERT INTO books (id, pub_date, is_bestseller, slug, title, image, author_id, description, price, discount)
VALUES
    (26, '2022-12-28', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Memories of Matsuko (Kiraware Matsuko no isshô)', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Memories of Matsuko (Kiraware Matsuko no isshô)', 'http://dummyimage.com/208x100.png/dddddd/000000', 42, 'vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae duis faucibus', 6040, 0),
    (27, '2022-09-18', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Angels with Dirty Faces', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Angels with Dirty Faces', 'http://dummyimage.com/213x100.png/cc0000/ffffff', 9, 'id turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue vel accumsan tellus nisi eu orci', 432, 16),
    (28, '2022-05-15', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('I Are You, You Am Me (Tenkosei)', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'I Are You, You Am Me (Tenkosei)', 'http://dummyimage.com/149x100.png/ff4444/ffffff', 20, 'vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque', 7820, 5),
    (29, '2023-03-12', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Rosie', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Rosie', 'http://dummyimage.com/159x100.png/ff4444/ffffff', 30, 'molestie nibh in lectus pellentesque at nulla suspendisse potenti cras in purus eu', 5606, 3),
    (30, '2022-09-12', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Bride with White Hair, The (Bai fa mo nu zhuan)', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Bride with White Hair, The (Bai fa mo nu zhuan)', 'http://dummyimage.com/237x100.png/dddddd/000000', 15, 'mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus', 7106, 19);
INSERT INTO books (id, pub_date, is_bestseller, slug, title, image, author_id, description, price, discount)
VALUES
    (31, '2022-12-25', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Borrowers, The', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Borrowers, The', 'http://dummyimage.com/202x100.png/ff4444/ffffff', 40, 'tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque', 1614, 11),
    (32, '2022-09-10', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Christmas Carol, A', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Christmas Carol, A', 'http://dummyimage.com/236x100.png/cc0000/ffffff', 23, 'fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat', 9589, 6),
    (33, '2023-02-20', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('King of Hearts', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'King of Hearts', 'http://dummyimage.com/190x100.png/ff4444/ffffff', 23, 'quis odio consequat varius integer ac leo pellentesque ultrices mattis odio donec vitae nisi nam', 8571, 4),
    (34, '2022-06-19', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Sun Kissed', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Sun Kissed', 'http://dummyimage.com/250x100.png/cc0000/ffffff', 3, 'quis lectus suspendisse potenti in eleifend quam a odio in hac habitasse platea dictumst maecenas ut massa quis augue', 7462, 9),
    (35, '2022-05-10', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Trail of the Pink Panther', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Trail of the Pink Panther', 'http://dummyimage.com/244x100.png/5fa2dd/ffffff', 9, 'dapibus augue vel accumsan tellus nisi eu orci mauris lacinia sapien quis libero nullam sit amet turpis elementum ligula vehicula', 6232, 15);
INSERT INTO books (id, pub_date, is_bestseller, slug, title, image, author_id, description, price, discount)
VALUES
    (36, '2022-12-27', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Murder Most Foul', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Murder Most Foul', 'http://dummyimage.com/114x100.png/dddddd/000000', 6, 'quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio', 9064, 9),
    (37, '2022-05-24', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Black Widow', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Black Widow', 'http://dummyimage.com/200x100.png/5fa2dd/ffffff', 6, 'sit amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere', 8930, 18),
    (38, '2022-10-21', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Big Trouble', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Big Trouble', 'http://dummyimage.com/231x100.png/5fa2dd/ffffff', 46, 'bibendum morbi non quam nec dui luctus rutrum nulla tellus in sagittis dui vel', 4200, 4),
    (39, '2022-08-28', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Airport', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Airport', 'http://dummyimage.com/129x100.png/dddddd/000000', 29, 'auctor sed tristique in tempus sit amet sem fusce consequat nulla nisl nunc nisl duis bibendum felis sed', 6047, 7),
    (40, '2022-05-31', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Cherry Crush', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Cherry Crush', 'http://dummyimage.com/141x100.png/cc0000/ffffff', 8, 'at vulputate vitae nisl aenean lectus pellentesque eget nunc donec', 8634, 3);
INSERT INTO books (id, pub_date, is_bestseller, slug, title, image, author_id, description, price, discount)
VALUES
    (41, '2022-08-25', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Passport to Pimlico', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Passport to Pimlico', 'http://dummyimage.com/104x100.png/dddddd/000000', 47, 'velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel nulla eget eros elementum pellentesque', 5547, 11),
    (42, '2023-04-02', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Juarez', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Juarez', 'http://dummyimage.com/121x100.png/5fa2dd/ffffff', 27, 'purus phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id consequat', 2131, 4),
    (43, '2022-07-29', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Perils of Pauline, The', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Perils of Pauline, The', 'http://dummyimage.com/139x100.png/5fa2dd/ffffff', 35, 'eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium', 4145, 6),
    (44, '2022-06-20', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Costa Brava', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Costa Brava', 'http://dummyimage.com/207x100.png/ff4444/ffffff', 5, 'dapibus dolor vel est donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est lacinia', 186, 18),
    (45, '2022-11-27', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('No Logo', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'No Logo', 'http://dummyimage.com/214x100.png/5fa2dd/ffffff', 7, 'ut massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit', 2594, 18);
INSERT INTO books (id, pub_date, is_bestseller, slug, title, image, author_id, description, price, discount)
VALUES
    (46, '2022-07-17', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Rabbit', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Rabbit', 'http://dummyimage.com/118x100.png/ff4444/ffffff', 32, 'ut massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus', 8660, 4),
    (47, '2022-12-21', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Reasonable Doubt', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Reasonable Doubt', 'http://dummyimage.com/157x100.png/5fa2dd/ffffff', 4, 'rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer', 5421, 17),
    (48, '2022-11-22', 1, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Ballad of Nessie, The', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Ballad of Nessie, The', 'http://dummyimage.com/248x100.png/5fa2dd/ffffff', 43, 'pede venenatis non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus', 3357, 5),
    (49, '2023-01-24', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('Other Shore, The', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'Other Shore, The', 'http://dummyimage.com/222x100.png/ff4444/ffffff', 7, 'varius ut blandit non interdum in ante vestibulum ante ipsum', 4660, 8),
    (50, '2022-09-08', 0, REPLACE(LOWER(TRIM(REGEXP_REPLACE('For Ellen', '[^[:alnum:] ]', '', 'g'))), ' ', '-'), 'For Ellen', 'http://dummyimage.com/204x100.png/ff4444/ffffff', 40, 'amet turpis elementum ligula vehicula consequat morbi a ipsum integer a nibh in quis', 7132, 3);

UPDATE authors
SET photo = CONCAT('https://picsum.photos/200/200?random=', FLOOR(RANDOM() * 10000))
WHERE photo IS NOT NULL;

UPDATE books
SET image = CONCAT('https://picsum.photos/300/500?random=', FLOOR(RANDOM() * 10000))
WHERE image IS NOT NULL;

INSERT INTO ratings (book_id, grade)
SELECT
    FLOOR(1 + RANDOM() * 50)::integer AS book_id,
    FLOOR(1 + RANDOM() * 5)::integer AS grade
FROM
    generate_series(1, 100) AS s;

UPDATE authors
SET description = 'Betsey Lamputt was born in London in 1980. She studied literature and creative writing at the University of East Anglia and went on to publish her first novel, "The Long Way Home," in 2005. Since then, she has written several other books, including the critically acclaimed "The Last Time I Saw You," which was shortlisted for the Booker Prize in 2012.Spoiler-HideBetsey Lamputt was born in London in 1980. She studied literature and creative writing at the University of East Anglia and went on to publish her first novel, "The Long Way Home," in 2005. Since then, she has written several other books, including the critically acclaimed "The Last Time I Saw You," which was shortlisted for the Booker Prize in 2012.'
WHERE id = 1;

UPDATE authors
SET description = 'Libbie Walliker is a Scottish author born in 1975. She studied English literature at the University of Edinburgh and went on to complete a Masters in Creative Writing at the University of Glasgow.Spoiler-Hide Her debut novel, "The Inheritance," was published in 2009 and won the Saltire First Book of the Year Award. She has since published several other novels, including "The Last Will and Testament of Lizzie Blythe" and "The Lost Girls of Foxfield Hall."'
WHERE id = 2;

UPDATE authors
SET description = 'Rowland Cluett is a Canadian writer born in 1960. He studied English literature at McGill University and went on to complete a PhD in Comparative Literature at the University of Toronto.Spoiler-Hide He has published several novels, including "The End of the World as We Know It" and "The Birds That Never Flew." His writing often deals with themes of identity, memory, and loss.'
WHERE id = 3;

UPDATE authors
SET description = 'Benedick Ferres was born in Dublin in 1972. He studied philosophy and theology at Trinity College Dublin and went on to complete a Masters in Creative Writing at the University of Edinburgh.Spoiler-Hide He is the author of several novels, including "The Dead House" and "The Island of Lost Souls." His writing often explores the darker side of human nature.'
WHERE id = 4;

UPDATE authors
SET description = 'Daffi Boog is an American author born in 1985. She studied creative writing at the University of Iowa and went on to publish her first novel, "The Girl Who Fell from the Sky," in 2010.Spoiler-Hide She has since written several other books, including the bestselling "The House of Small Shadows."'
WHERE id = 5;

UPDATE authors
SET description = 'Mic Filkov is a Russian writer born in 1987. He studied physics at Moscow State University and went on to complete a PhD in Computational Neuroscience at the University of Edinburgh.Spoiler-Hide His first novel, "The Quantum Thief," was published in 2010 and won the John W. Campbell Award for Best New Writer. He has since written several other books, including "The Fractal Prince" and "The Causal Angel."'
WHERE id = 6;

UPDATE authors
SET description = 'Joletta Staynes is a British author born in 1978. She studied history at the University of Oxford and went on to complete a Masters in Creative Writing at the University of East Anglia.Spoiler-Hide Her debut novel, "The Invention of Wings," was published in 2014 and was a New York Times bestseller. She has since written several other books, including "The Mermaid and Mrs. Hancock" and "The Confessions of Frannie Langton."'
WHERE id = 7;

UPDATE authors
SET description = 'Suki Tinkham is an Australian author born in 1982. She studied creative writing at the University of Melbourne and went on to publish her first novel, "The Other Side of the World," in 2015.Spoiler-Hide She has since written several other books, including "The Hollow Bones" and "The Invisible Thread."'
WHERE id = 8;

UPDATE authors
SET description = 'Leandra Peddel is a Dutch writer born in 1984. She studied Dutch literature at the University of Amsterdam and went on to complete a Masters in Creative Writing at the University of Edinburgh.Spoiler-Hide Her debut novel, "The Nightingale''s Nest," was published in 2013 and won the Dioraphte Literature Prize. She has since written several other books, including "The Secret of the Ninth Planet" and "The Vanishing Stair."'
WHERE id = 9;

UPDATE authors
SET description = 'Seka Bonn is a Swedish author born in 1972. She studied psychology at Stockholm University and went on to complete a PhD in Neuroscience at the Karolinska Institute.Spoiler-Hide Her debut novel, "The Memory Keeper," was published in 2016 and was a bestseller in Sweden. She has since written several other books, including "The Forgotten Child" and "The Lost Sister."'
WHERE id = 10;

UPDATE authors
SET description = 'Georgie St. John is a British writer born in 1986. She studied English literature at the University of Cambridge and went on to complete a Masters in Creative Writing at the University of East Anglia.Spoiler-Hide Her debut novel, "The Silence," was published in 2019 and was a Sunday Times bestseller. She has since written several other books, including "The House of Whispers" and "The Darkest Night."'
WHERE id = 11;

UPDATE authors
SET description = 'Jourdain Willoughby is an American author born in 1990. He studied creative writing at Yale University and went on to publish his first novel, "The Other Side of the River," in 2017.Spoiler-Hide He has since written several other books, including "The End of the World Is a Fine and Private Place" and "The Dead Will Rise Again."'
WHERE id = 12;

UPDATE authors
SET description = 'Claudianus Idell is a Dutch writer born in 1983. He studied philosophy at the University of Amsterdam and went on to complete a PhD in Creative Writing at the University of Edinburgh.Spoiler-Hide His debut novel, "The Book of the Lost," was published in 2015 and was a bestseller in the Netherlands. He has since written several other books, including "The Last Days of Summer" and "The Age of Wonders."'
WHERE id = 13;

UPDATE authors
SET description = 'Salim McDuffy is an American author born in 1984. He studied English literature at Harvard University and went on to complete a Masters in Creative Writing at the University of Iowa.Spoiler-Hide His debut novel, "The Language of Stones," was published in 2012 and was a finalist for the National Book Award. He has since written several other books, including "The Island of Memory" and "The Road to Damascus."'
WHERE id = 14;

UPDATE authors
SET description = 'Anthea Elgram is a British writer born in 1977. She studied classics at the University of Oxford and went on to complete a Masters in Creative Writing at the University of East Anglia.Spoiler-Hide Her debut novel, "The Gorgon''s Head," was published in 2014 and was a bestseller in the UK. She has since written several other books, including "The Hidden Oracle" and "The Battle of the Labyrinth."'
WHERE id = 15;

UPDATE authors
SET description = 'Simeon Holbarrow is a Canadian author born in 1981. He studied English literature at the University of Toronto and went on to complete a PhD in Creative Writing at the University of British Columbia.Spoiler-Hide His debut novel, "The Silence of the Wolves," was published in 2018 and was a finalist for the Giller Prize. He has since written several other books, including "The Last Night of the World" and "The Light Between Worlds."'
WHERE id = 16;

UPDATE authors
SET description = 'Deana Ringham is a British writer born in 1980. She studied English literature at the University of Cambridge and went on to complete a Masters in Creative Writing at the University of East Anglia.Spoiler-Hide Her debut novel, "The House of Dust," was published in 2015 and was a bestseller in the UK. She has since written several other books, including "The Sea Change" and "The Darkling Thrush."'
WHERE id = 17;

UPDATE authors
SET description = 'Johnathon Arnould is a French author born in 1985. He studied philosophy at the Sorbonne and went on to complete a PhD in Creative Writing at the University of Edinburgh.Spoiler-Hide His debut novel, "The Night Watchman," was published in 2017 and was a bestseller in France'
WHERE id = 18;

UPDATE authors
SET description = 'Maximilien Wannan is a German writer born in 1982. He studied German literature at the University of Berlin and went on to complete a Masters in Creative Writing at the University of Munich.Spoiler-Hide His debut novel, "The City of Shadows," was published in 2014 and was a bestseller in Germany. He has since written several other books, including "The Last Kingdom" and "The Crown of Thorns."'
WHERE id = 19;

UPDATE authors
SET description = 'Addie Turbard is an American author born in 1978. She studied creative writing at the University of California, Los Angeles and went on to publish her first novel, "The Secret Life of Bees," in 2002.Spoiler-Hide The book became a bestseller and was later adapted into a film. She has since written several other books, including "The Mermaid Chair" and "The Invention of Wings."'
WHERE id = 20;

UPDATE authors
SET description = 'Gothart Fike is a Dutch writer born in 1987. He studied Dutch literature at the University of Amsterdam and went on to complete a Masters in Creative Writing at the University of Edinburgh.Spoiler-Hide His debut novel, "The Anatomy Lesson," was published in 2013 and won the Dutch Literature Prize. He has since written several other books, including "The Last Rembrandt" and "The Golden Age."'
WHERE id = 21;

UPDATE authors
SET description = 'Bertie Buddell is a British author born in 1980. He studied English literature at the University of Oxford and went on to complete a Masters in Creative Writing at the University of East Anglia.Spoiler-Hide His debut novel, "The Island of Dr. Moreau," was published in 2016 and was a bestseller in the UK. He has since written several other books, including "The Time Machine" and "The War of the Worlds."'
WHERE id = 22;

UPDATE authors
SET description = 'Henrik Schurig is a German writer born in 1984. He studied German literature at the University of Heidelberg and went on to complete a PhD in Creative Writing at the University of Edinburgh.Spoiler-Hide His debut novel, "The Lost Kingdom," was published in 2018 and was a bestseller in Germany. He has since written several other books, including "The Last Empress" and "The Thousand-Year Reich."'
WHERE id = 23;

UPDATE authors
SET description = 'Raphaela Hovenden is a British author born in 1989. She studied English literature at the University of Cambridge and went on to complete a Masters in Creative Writing at the University of East Anglia.Spoiler-Hide Her debut novel, "The Nightingale''s Song," was published in 2016 and was a bestseller in the UK. She has since written several other books, including "The House of Mirth" and "The Age of Innocence."'
WHERE id = 24;

UPDATE authors
SET description = 'Frans Gilhooly is an Irish writer born in 1981. He studied Irish literature at Trinity College Dublin and went on to complete a Masters in Creative Writing at the University of Edinburgh.Spoiler-Hide His debut novel, "The Irish Princess," was published in 2017 and was a bestseller in Ireland. He has since written several other books, including "The Last King of Ireland" and "The Battle of Clontarf."'
WHERE id = 25;

UPDATE authors
SET description = 'Dale Ranger is an Australian author born in 1975. He studied creative writing at the University of Melbourne and went on to publish his first novel, "The Broken Road," in 2004.Spoiler-Hide He has since written several other books, including "The Secret Life of Us" and "The Resurrectionist."'
WHERE id = 26;

UPDATE authors
SET description = 'Anna-maria Leetham is a Canadian writer born in 1982. She studied English literature at McGill University and went on to complete a Masters in Creative Writing at the University of British Columbia.Spoiler-Hide Her debut novel, "The Water Will Hold You," was published in 2013 and was a bestseller in Canada. She has since written several other books, including "Where the River Ends" and "The Lightkeeper''s Daughter."'
WHERE id = 27;

UPDATE authors
SET description = 'Pet Calverley is a British author born in 1979. She studied English literature at the University of Cambridge and went on to complete a Masters in Creative Writing at the University of East Anglia.Spoiler-Hide Her debut novel, "The House at Riverton," was published in 2008 and was a bestseller in the UK. She has since written several other books, including "The Secret Keeper" and "The Clockmaker''s Daughter."'
WHERE id = 28;

UPDATE authors
SET description = 'Normy Kelberer is an American writer born in 1983. She studied English literature at Harvard University and went on to complete a Masters in Creative Writing at the University of Iowa.Spoiler-Hide Her debut novel, "The Hollow Bones," was published in 2019 and was a finalist for the Pulitzer Prize. She has since written several other books, including "The Broken Road" and "The Other Side of the River."'
WHERE id = 29;

UPDATE authors
SET description = 'Pauline Chooter is a British author born in 1985. She studied creative writing at the University of Manchester and went on to publish her first novel, "The Forgotten Garden," in 2008.Spoiler-Hide The book became a bestseller and was later adapted into a film. She has since written several other books, including "The Secret Keeper" and "The Lake House."'
WHERE id = 30;

UPDATE authors
SET description = 'Ardene O''Day is an American author born in 1987. She studied English literature at Yale University and went on to complete a Masters in Creative Writing at the University of Iowa.Spoiler-Hide Her debut novel, "The House of Mirth," was published in 2014 and was a bestseller in the US. She has since written several other books, including "The Age of Innocence" and "The Custom of the Country."'
WHERE id = 31;

UPDATE authors
SET description = 'Toiboid Sandy is an Irish writer born in 1979. He studied Irish literature at Trinity College Dublin and went on to complete a PhD in Creative Writing at the University of Edinburgh.Spoiler-Hide His debut novel, "The Last King of Ireland," was published in 2016 and was a bestseller in Ireland. He has since written several other books, including "The Battle of Clontarf" and "The Great Hunger."'
WHERE id = 32;

UPDATE authors
SET description = 'Annetta Swetman is a British author born in 1982. She studied English literature at the University of Oxford and went on to complete a Masters in Creative Writing at the University of East Anglia.Spoiler-Hide Her debut novel, "The Inheritance," was published in 2012 and was a bestseller in the UK. She has since written several other books, including "The Lost Sister" and "The Darkest Hour."'
WHERE id = 33;

UPDATE authors
SET description = 'Tomlin Hansberry is an American writer born in 1980. She studied creative writing at the University of California, Los Angeles and went on to publish her first novel, "The House of Mirth," in 2006.Spoiler-Hide The book became a bestseller and was later adapted into a film. She has since written several other books, including "The Age of Innocence" and "The Custom of the Country."'
WHERE id = 34;

UPDATE authors
SET description = 'Sabina Tizzard is a Canadian author born in 1981. She studied English literature at McGill University and went on to complete a Masters in Creative Writing at the University of British Columbia.Spoiler-Hide Her debut novel, "The Secret Keeper," was published in 2012 and was a bestseller in Canada. She has since written several other books, including "The Clockmaker''s Daughter" and "The Winter Sea."'
WHERE id = 35;

UPDATE authors
SET description = 'Eleanore Melby is a Norwegian writer born in 1978. She studied Norwegian literature at the University of Oslo and went on to complete a Masters in Creative Writing at the University of Edinburgh.Spoiler-Hide Her debut novel, "The Snow Child," was published in 2012 and was a bestseller in Norway. The book was also a finalist for the Pulitzer Prize and was later adapted into a stage play. She has since written several other books, including "The Bright Edge of the World" and "To the Bright Edge of the World."'
WHERE id = 36;

UPDATE authors
SET description = 'Keith Testro is an Australian author born in 1984. He studied creative writing at the University of Melbourne and went on to publish his first novel, "The Road to Winter," in 2016.Spoiler-Hide The book became a bestseller in Australia and was later adapted into a film. He has since written several other books, including "The Wild Blue Yonder" and "The End of the World is a Fine and Private Place."'
WHERE id = 37;

UPDATE authors
SET description = 'Albina Stampe is a Danish writer born in 1982. She studied Danish literature at the University of Copenhagen and went on to complete a PhD in Creative Writing at the University of Edinburgh.Spoiler-Hide Her debut novel, "The Memory Keeper," was published in 2015 and was a bestseller in Denmark. She has since written several other books, including "The Lost Sister" and "The Secret History."'
WHERE id = 38;

UPDATE authors
SET description = 'Tori Frushard is an American author born in 1977. She studied creative writing at the University of Iowa and went on to publish her first novel, "The Immortalists," in 2018.Spoiler-Hide The book became a bestseller in the US and was later adapted into a TV series. She has since written several other books, including "The Age of Miracles" and "The Dreamers."'
WHERE id = 39;

UPDATE authors
SET description = 'Dalt Plenderleith is a Scottish writer born in 1981. He studied English literature at the University of Edinburgh and went on to complete a Masters in Creative Writing at the University of Glasgow.Spoiler-Hide His debut novel, "The Ghost of the Mary Celeste," was published in 2014 and was a bestseller in the UK. He has since written several other books, including "The Loch Ness Monster" and "The Secret of Loch Morar."'
WHERE id = 40;

UPDATE authors
SET description = 'Rosita Spira is a Dutch author born in 1985. She studied Dutch literature at the University of Amsterdam and went on to complete a Masters in Creative Writing at the University of Edinburgh.Spoiler-Hide Her debut novel, "The Anatomy Lesson," was published in 2015 and was a bestseller in the Netherlands. She has since written several other books, including "The Last Rembrandt" and "The Dutch Republic."'
WHERE id = 41;

UPDATE authors
SET description = 'Ramonda Pedlow is an American writer born in 1979. She studied creative writing at the University of California, Los Angeles and went on to publish her first novel, "The Power of the Dog," in 2005.Spoiler-Hide The book became a bestseller and was later adapted into a film. She has since written several other books, including "The Cartel" and "The Border."'
WHERE id = 42;

UPDATE authors
SET description = 'Davidson Broadway is a British author born in 1980. He studied English literature at the University of Cambridge and went on to complete a Masters in Creative Writing at the University of East Anglia.Spoiler-Hide His debut novel, "The Silent Companions," was published in 2017 and was a bestseller in the UK. He has since written several other books, including "The Craftsman" and "The Glass Woman."'
WHERE id = 43;

UPDATE authors
SET description = 'Sigmund Cunniff is an Irish writer born in 1982. He studied Irish literature at Trinity College Dublin and went on to complete a PhD in Creative Writing at the University of Edinburgh.Spoiler-Hide His debut novel, "The Lost Kingdom," was published in 2015 and was a bestseller in Ireland. He has since written several other books, including "The Last King of Ireland" and "The Great Hunger."'
WHERE id = 44;

UPDATE authors
SET description = 'She studied English literature at the University of Toronto and went on to complete a Masters in Creative Writing at the University of British Columbia.Spoiler-Hide Her debut novel, "The Piano Teacher," was published in 2010 and was a bestseller in Canada. She has since written several other books, including "The Forgotten Garden" and "The Lake House."'
WHERE id = 45;

UPDATE authors
SET description = 'Kristine Shadwick is an American writer born in 1983. She studied creative writing at the University of Iowa and went on to publish her first novel, "The Lost Girls of Paris," in 2019.Spoiler-Hide The book became a bestseller in the US and was later adapted into a TV series. She has since written several other books, including "The Flight Girls" and "The Women with Silver Wings."'
WHERE id = 46;

UPDATE authors
SET description = 'Deloria Lowles is a British author born in 1980. She studied English literature at the University of Oxford and went on to complete a Masters in Creative Writing at the University of East Anglia.Spoiler-Hide Her debut novel, "The End of Everything," was published in 2011 and was a bestseller in the UK. She has since written several other books, including "The Vanishing Half" and "The Mothers."'
WHERE id = 47;

UPDATE authors
SET description = 'Stephi Baldery is an Australian writer born in 1985. She studied creative writing at the University of Melbourne and went on to publish her first novel, "The Farm," in 2018.Spoiler-Hide The book became a bestseller in Australia and was later adapted into a TV series. She has since written several other books, including "The Island" and "The Weekend."'
WHERE id = 48;

UPDATE authors
SET description = 'Johann Bernardeau is a French author born in 1987. He studied French literature at the University of Paris and went on to complete a Masters in Creative Writing at the University of Edinburgh.Spoiler-Hide His debut novel, "The King of Prussia," was published in 2016 and was a bestseller in France. He has since written several other books, including "The Last Napoleon" and "The French Empire."'
WHERE id = 49;

UPDATE authors
SET description = 'Annie Chellam is an Indian writer born in 1979. She studied creative writing at the University of Delhi and went on to publish her first novel, "The Palace of Illusions," in 2008.Spoiler-Hide The book became a bestseller in India and was later adapted into a TV series. She has since written several other books, including "The Forest of Enchantments" and "The Book of Fate."'
WHERE id = 50;

BEGIN;
DO
$$
    DECLARE
        i integer := 1;
    BEGIN
        WHILE i <= 51 LOOP
                INSERT INTO tags (id, tag_name) VALUES (i, 'Tag' || i::text);
                i := i + 1;
            END LOOP;
    END;
$$
;
COMMIT;

-- Замена тегов
UPDATE tags SET tag_name = 'современная литература' WHERE id = 1;
UPDATE tags SET tag_name = 'классическая литература' WHERE id = 2;
UPDATE tags SET tag_name = 'зарубежная литература' WHERE id = 3;
UPDATE tags SET tag_name = 'фэнтези' WHERE id = 4;
UPDATE tags SET tag_name = 'английская литература' WHERE id = 5;
UPDATE tags SET tag_name = 'русская литература' WHERE id = 6;
UPDATE tags SET tag_name = 'американская литература' WHERE id = 7;
UPDATE tags SET tag_name = 'фантастика' WHERE id = 8;
UPDATE tags SET tag_name = 'детская литература' WHERE id = 9;
UPDATE tags SET tag_name = 'детектив' WHERE id = 10;
UPDATE tags SET tag_name = 'любовь' WHERE id = 11;
UPDATE tags SET tag_name = 'мистика' WHERE id = 12;
UPDATE tags SET tag_name = 'юмор' WHERE id = 13;
UPDATE tags SET tag_name = 'приключения' WHERE id = 14;
UPDATE tags SET tag_name = 'сказка' WHERE id = 15;
UPDATE tags SET tag_name = 'французская литература' WHERE id = 16;
UPDATE tags SET tag_name = 'англия' WHERE id = 17;
UPDATE tags SET tag_name = 'художественная литература' WHERE id = 18;
UPDATE tags SET tag_name = 'психология' WHERE id = 19;
UPDATE tags SET tag_name = 'философия' WHERE id = 20;
UPDATE tags SET tag_name = 'экранизировано' WHERE id = 21;
UPDATE tags SET tag_name = 'любимое' WHERE id = 22;
UPDATE tags SET tag_name = 'антиутопия' WHERE id = 23;
UPDATE tags SET tag_name = 'books' WHERE id = 24;
UPDATE tags SET tag_name = 'you must read before you die' WHERE id = 25;
UPDATE tags SET tag_name = 'роман' WHERE id = 26;
UPDATE tags SET tag_name = 'франция' WHERE id = 27;
UPDATE tags SET tag_name = 'дайте две' WHERE id = 28;
UPDATE tags SET tag_name = 'young' WHERE id = 29;
UPDATE tags SET tag_name = 'adult' WHERE id = 30;
UPDATE tags SET tag_name = 'война' WHERE id = 31;
UPDATE tags SET tag_name = 'история' WHERE id = 32;
UPDATE tags SET tag_name = 'флэшмоб 2016' WHERE id = 33;
UPDATE tags SET tag_name = 'флэшмоб 2015' WHERE id = 34;
UPDATE tags SET tag_name = 'детство' WHERE id = 35;
UPDATE tags SET tag_name = 'жизнь' WHERE id = 36;
UPDATE tags SET tag_name = 'драма' WHERE id = 37;
UPDATE tags SET tag_name = 'россия' WHERE id = 38;
UPDATE tags SET tag_name = 'книжное путешествие' WHERE id = 39;
UPDATE tags SET tag_name = 'советская литература' WHERE id = 40;
UPDATE tags SET tag_name = 'триллер' WHERE id = 41;
UPDATE tags SET tag_name = 'америка' WHERE id = 42;
UPDATE tags SET tag_name = 'немецкая литература' WHERE id = 43;
UPDATE tags SET tag_name = 'вампиры' WHERE id = 44;
UPDATE tags SET tag_name = 'школьная программа' WHERE id = 45;
UPDATE tags SET tag_name = 'рассказы' WHERE id = 46;
UPDATE tags SET tag_name = 'магия' WHERE id = 47;
UPDATE tags SET tag_name = 'дети' WHERE id = 48;
UPDATE tags SET tag_name = 'русская классика' WHERE id = 49;
UPDATE tags SET tag_name = 'биография' WHERE id = 50;
UPDATE tags SET tag_name = 'english' WHERE id = 51;


DO
$$
    DECLARE
        book_counter integer;
        random_tag_id integer;
    BEGIN
        FOR book_counter IN 1..50 LOOP
                FOR i IN 1..4 LOOP
                        SELECT floor(random() * 50 + 1)::integer INTO random_tag_id;
                        INSERT INTO book_tags (book_id, tag_id) VALUES (book_counter, random_tag_id);
                    END LOOP;
            END LOOP;
    END;
$$;


UPDATE books
SET discount = CASE
                   WHEN random() < 0.5 THEN 0  -- 50% шанс, что скидка будет равна 0
                   ELSE price * (0.05 + random() * 0.15)  -- Случайное значение между 5% и 20%
    END;

-- Mock data for `genre` table
INSERT INTO genre (id, parent_id, slug, name) VALUES
                                                  (1, NULL, 'fiction', 'Fiction'),
                                                  (2, 1, 'fantasy', 'Fantasy'),
                                                  (3, 2, 'high-fantasy', 'High Fantasy'),
                                                  (4, 2, 'low-fantasy', 'Low Fantasy'),

                                                  (5, 1, 'mystery', 'Mystery'),
                                                  (6, 5, 'cozy-mystery', 'Cozy Mystery'),
                                                  (7, 5, 'crime-mystery', 'Crime Mystery'),

                                                  (8, 1, 'historical', 'Historical Fiction'),
                                                  (9, 8, 'ancient-history', 'Ancient History'),
                                                  (10, 8, 'modern-history', 'Modern History'),

                                                  (11, NULL, 'non-fiction', 'Non-Fiction'),
                                                  (12, 11, 'biography', 'Biography'),
                                                  (13, 11, 'self-help', 'Self-help'),
                                                  (14, 13, 'career', 'Career'),
                                                  (15, 13, 'relationships', 'Relationships'),

                                                  (16, 11, 'true-crime', 'True Crime'),
                                                  (17, 16, 'heists', 'Heists'),
                                                  (18, 16, 'murder', 'Murder'),

                                                  (19, NULL, 'science', 'Science'),
                                                  (20, 19, 'physics', 'Physics'),
                                                  (21, 19, 'biology', 'Biology');




-- Mock data for `book2genre` table for PostgreSQL
DO $$
    DECLARE
        i integer := 1;
        random_genre integer;
    BEGIN
        WHILE i <= 50 LOOP
                SELECT floor(random() * 21 + 1)::integer INTO random_genre;  -- Update the range to match new genre IDs
                INSERT INTO book2genre (book_id, genre_id) VALUES (i, random_genre);
                i := i + 1;
            END LOOP;
    END $$;

UPDATE book2genre
SET genre_id = 2
WHERE genre_id = 1;

UPDATE book2genre
SET genre_id = 12
WHERE genre_id = 11;

UPDATE book2genre
SET genre_id = 20
WHERE genre_id = 19;

