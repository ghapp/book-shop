package com.example.MyBookShopApp.genre;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GenreDTO {
    private String name;
    private Integer bookCount;
    private Integer id;
    private Integer parentId;
    private String slug;
    private List<GenreDTO> children;
}

