package com.example.MyBookShopApp.genre;

import com.example.MyBookShopApp.book.links.Book2GenreEntity;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "genre")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GenreEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(columnDefinition = "INT")
    private Integer parentId;

    @Column(columnDefinition = "VARCHAR(255) NOT NULL")
    private String slug;

    @Column(columnDefinition = "VARCHAR(255) NOT NULL")
    private String name;

    @Transient
    private List<GenreEntity> children = new ArrayList<>();

    // количество книг в жанре
    @Transient
    private Integer amount;

    @OneToMany(mappedBy = "genre")
    private List<Book2GenreEntity> book2Genres;


    public void setChildren(List<GenreEntity> children) {
        this.children = children;
    }
}
