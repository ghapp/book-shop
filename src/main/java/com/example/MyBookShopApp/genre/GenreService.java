package com.example.MyBookShopApp.genre;

import com.example.MyBookShopApp.book.links.Book2GenreEntity;
import com.example.MyBookShopApp.dto.BookDto;
import com.example.MyBookShopApp.repo.Book2genreRepository;
import com.example.MyBookShopApp.repo.BookRepository;
import com.example.MyBookShopApp.repo.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class GenreService {
    @Autowired
    private GenreRepository genreRepository;
    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private Book2genreRepository book2genreRepository;

    public List<GenreEntity> getAllGenresWithAmount() {
        List<GenreEntity> genres = genreRepository.findAll();
        Map<Integer, GenreEntity> genreMap = new HashMap<>();

        // Создаем мапу для быстрого доступа
        for (GenreEntity genre : genres) {
            genreMap.put(genre.getId(), genre);
        }

        // Устанавливаем дочерние элементы
        for (GenreEntity genre : genres) {
            if (genre.getParentId() != null) {
                GenreEntity parent = genreMap.get(genre.getParentId());
                if (parent != null) {
                    if (parent.getChildren() == null) {
                        parent.setChildren(new ArrayList<>());
                    }
                    parent.getChildren().add(genre);
                }
            }
        }

        // Расчет количества и фильтрация
        List<GenreEntity> rootGenres = new ArrayList<>();
        for (GenreEntity genre : genres) {
            if (genre.getParentId() == null) {
                calculateAmount(genre);
                rootGenres.add(genre);
            }
        }

        return rootGenres;
    }

    public void calculateAmount(GenreEntity genre) {
        Long count = book2genreRepository.countByGenreId(genre.getId());  // подсчет книг в этом жанре
        for (GenreEntity child : genre.getChildren()) {
            calculateAmount(child);  // рекурсивный вызов для поджанров
            count += child.getAmount();
        }
        genre.setAmount(count.intValue());
    }

    public GenreDTO fromEntityToDto(GenreEntity entity) {
        GenreDTO dto = new GenreDTO();
        dto.setName(entity.getName());
        dto.setBookCount(entity.getAmount());
        dto.setId(entity.getId());
        dto.setParentId(entity.getParentId());
        dto.setSlug(entity.getSlug());

        if (entity.getChildren() != null) {
            dto.setChildren(entity.getChildren().stream()
                    .map(this::fromEntityToDto)
                    .collect(Collectors.toList()));
        }

        return dto;
    }

    public void sortGenresRecursively(GenreDTO genre) {
        if (genre.getChildren() != null) {
            genre.getChildren().sort((g1, g2) -> g2.getBookCount() - g1.getBookCount());
            for (GenreDTO child : genre.getChildren()) {
                sortGenresRecursively(child);
            }
        }
    }

    public List<GenreDTO> getAllGenresWithAmountForFrontend() {
        List<GenreEntity> genres = getAllGenresWithAmount();
        List<GenreDTO> sortedList = genres.stream()
                .map(this::fromEntityToDto)
                .sorted((g1, g2) -> g2.getBookCount() - g1.getBookCount())
                .collect(Collectors.toList());

        for (GenreDTO genre : sortedList) {
            sortGenresRecursively(genre);
        }

        return sortedList;
    }

    public String getGenreNameBySlug(String slug) {
        Optional<GenreEntity> genre = genreRepository.findBySlug(slug);
        return genre.map(GenreEntity::getName).orElse(null);
    }

    public List<BookDto> getListBooksByGenreSlug(String slug) {
        List<GenreDTO> allGenres = getAllGenresWithAmountForFrontend();
        Optional<GenreDTO> genreDTO = findGenreBySlug(allGenres, slug);
        if (genreDTO.isPresent()) {
            List<Book2GenreEntity> book2GenreEntities =
                    book2genreRepository.findByGenreId(genreDTO.get().getId());
            if (genreDTO.get().getChildren().isEmpty()) {
                return book2GenreEntities.stream().map(Book2GenreEntity::getBook)
                        .map(BookDto::new)
                        .collect(Collectors.toList());
            }
            for (GenreDTO child : genreDTO.get().getChildren()) {
                book2GenreEntities.addAll(book2genreRepository.findByGenreId(child.getId()));
            }
            return book2GenreEntities
                    .stream().map(Book2GenreEntity::getBook)
                    .map(BookDto::new)
                    .collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    public Page<BookDto> getPageBooksByGenreSlug(String slug, Integer offset, Integer limit) {


        List<GenreDTO> allGenres = getAllGenresWithAmountForFrontend();
        Optional<GenreDTO> genreDTO = findGenreBySlug(allGenres, slug);
        Pageable nextPage = PageRequest.of(offset, limit);
//        Pageable pageable = PageRequest.of(offset / limit, limit); // создаем объект pageable

        if (genreDTO.isPresent()) {
            // Собираем ID всех нужных жанров (родительский + дочерние)
            List<Integer> allGenreIds = new ArrayList<>();
            allGenreIds.add(genreDTO.get().getId());
            for (GenreDTO child : genreDTO.get().getChildren()) {
                allGenreIds.add(child.getId());
            }

            // Выполняем поиск книг по всем ID
            // Здесь вы должны адаптировать запрос в зависимости от того, что поддерживает ваш репозиторий
            Page<Book2GenreEntity> book2GenreEntities = book2genreRepository.findAllByIds(allGenreIds, nextPage);

            // Обработка дочерних жанров. Обратите внимание, что это будет неэффективно,
            // если у вас много дочерних жанров и много книг в каждом.
            List<Book2GenreEntity> modifiableList = new ArrayList<>(book2GenreEntities.getContent());

            for (GenreDTO child : genreDTO.get().getChildren()) {
                Page<Book2GenreEntity> childBook2GenreEntities = book2genreRepository.findByGenreId(child.getId(), nextPage);
                modifiableList.addAll(childBook2GenreEntities.getContent());
            }

            return book2GenreEntities.map(Book2GenreEntity::getBook).map(BookDto::new);
        } else {
            return Page.empty(); // возвращает пустую страницу
        }
    }


    // рекурсивный поиск жанра по slug
    public Optional<GenreDTO> findGenreBySlug(List<GenreDTO> genres, String slug) {

        for (GenreDTO genre : genres) {
            if (genre.getSlug().equalsIgnoreCase(slug)) {
                return Optional.of(genre);
            }
            if (genre.getChildren() != null) {
                Optional<GenreDTO> childGenre = findGenreBySlug(genre.getChildren(), slug);
                if (childGenre.isPresent()) {
                    return childGenre;
                }
            }
        }
        return Optional.empty();
    }

}
