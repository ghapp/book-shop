package com.example.MyBookShopApp;

import org.apache.log4j.BasicConfigurator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class MyBookShopAppApplication {

    public static void main(String[] args) {
        BasicConfigurator.configure();
        SpringApplication app = new SpringApplication(MyBookShopAppApplication.class);
        app.run(args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void onAppReady() {
        System.out.print("\n");
        System.out.print("######                        #####                       \n");
        System.out.print("#     #  ####   ####  #    # #     # #    #  ####  #####  \n");
        System.out.print("#     # #    # #    # #   #  #       #    # #    # #    # \n");
        System.out.print("######  #    # #    # ####    #####  ###### #    # #    # \n");
        System.out.print("#     # #    # #    # #  #         # #    # #    # #####  \n");
        System.out.print("#     # #    # #    # #   #  #     # #    # #    # #      \n");
        System.out.print("######   ####   ####  #    #  #####  #    #  ####  #      \n");
        System.out.print("\n");
    }

}
