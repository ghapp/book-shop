package com.example.MyBookShopApp.controllers;

import com.example.MyBookShopApp.book.BookService;
import com.example.MyBookShopApp.dto.BooksPageDto;
import com.example.MyBookShopApp.dto.SearchWordDto;
import com.example.MyBookShopApp.dto.BookDto;
import com.example.MyBookShopApp.genre.GenreService;
import com.example.MyBookShopApp.repo.TagRepository;
import lombok.RequiredArgsConstructor;
import org.apache.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
@Controller
@RequestMapping("/books")
@RequiredArgsConstructor
public class BookController {
    private final Logger logger = Logger.getLogger(BookController.class);
    private final BookService bookService;
    private final GenreService genreService;
    private final TagRepository tagRepository;

    /**
     * Метод возвращает страницу с новинками
     *
     * @param offset
     * @param limit
     * @param model
     * @return
     */
    @GetMapping("/recent")
    public String getRecentBooks(@RequestParam(required = false, defaultValue = "0") Integer offset,
                                 @RequestParam(required = false, defaultValue = "10") Integer limit,
                                 Model model) {
        logger.info("GET recent returns recent books");
        List<BookDto> books = bookService.getPageOfRecentBooksDto(offset, limit);
        SearchWordDto searchWordDto = new SearchWordDto();
        model.addAttribute("category", "Новинки");
        model.addAttribute("recentBooks", books);
        model.addAttribute("sourcePage", "recent");
        model.addAttribute("showMoreButton", books.size() == limit);
        model.addAttribute("searchWordDto", searchWordDto);
        return "books/recent";
    }

    /**
     * Метод подгружает следующую порцию книг на странице новинок
     * @param offset
     * @param limit
     * @param fromDate
     * @param endDate
     * @return
     */
    @GetMapping("/page/recent")
    public ResponseEntity<BooksPageDto> getPageRecentBooksPage(@RequestParam(required = false, defaultValue = "0") Integer offset,
                                                               @RequestParam(required = false, defaultValue = "10") Integer limit,
                                                               @RequestParam(required = false, name = "from") String fromDate,
                                                               @RequestParam(required = false, name = "to") String endDate) {
        logger.info("GET page of recent books");
        List<BookDto> recentBooks;
        if (fromDate == null) {
            fromDate = localDateToString(LocalDate.now().minusMonths(3));
            endDate = localDateToString(LocalDate.now());
        }
        recentBooks = bookService.getPageOfRecentBooksDtoInDateRange(
                stringToLocalDate(fromDate),
                stringToLocalDate(endDate),
                offset,
                limit
        ).getContent();
        return new ResponseEntity<>(
                new BooksPageDto(recentBooks), HttpStatus.OK);
    }

    /**
     * Метод возвращает страницу с популярными книгами (по рейтингу)
     * @param offset
     * @param limit
     * @param model
     * @return
     */
    @GetMapping("/popular")
    public String getPopularBooks(@RequestParam(required = false, defaultValue = "0") Integer offset,
                                  @RequestParam(required = false, defaultValue = "10") Integer limit,
                                  Model model) {
        logger.info("GET popular books");
        List<BookDto> books = bookService.getBooksDtoSortedByAverageRating(offset, limit);
        SearchWordDto searchWordDto = new SearchWordDto();
        model.addAttribute("category", "Популярное");
        model.addAttribute("popularBooks", books);
        model.addAttribute("sourcePage", "popular");
        model.addAttribute("showMoreButton", books.size() == limit);
        model.addAttribute("searchWordDto", searchWordDto);
        return "books/popular";
    }

    /**
     * Метод подгружает следующую порцию книг на странице популярных книг
     * @param offset
     * @param limit
     * @return
     */
    @GetMapping("/page/popular")
    public ResponseEntity<BooksPageDto> getPagePopularBooks(@RequestParam(required = false) Integer offset,
                                                            @RequestParam(required = false) Integer limit) {
        logger.info("GET page of popular books");
        return new ResponseEntity<>(new BooksPageDto(
                bookService.getBooksDtoSortedByAverageRating(offset, limit)),
                HttpStatus.OK);
    }

    /**
     * Метод возвращает страницу с выбранной книгой
     * @param slug
     * @param model
     * @return
     */
    @GetMapping("/book/{slug}")
    public String getBookBySlug(@PathVariable String slug, Model model) {
        logger.info("GET /books/SLUG returns book by slug: " + slug);

        BookDto book = bookService.getBookBySlug(slug);
        model.addAttribute("book", book);
        model.addAttribute("author", book.getAuthor());
        model.addAttribute("searchWordDto", new SearchWordDto());
        return "books/slug";
    }

    /**
     * Метод возвращает страницу с книгами выбранного автора
     * @param authorName
     * @param model
     * @return
     */
    @GetMapping("/author/{authorName}")
    public String getBooksByAuthor(@PathVariable String authorName, Model model) {
        logger.info("GET /author/author-name returns book by authorName: " + authorName);
        List<BookDto> books = bookService.getBooksByAuthor(authorName);
        model.addAttribute("books", books);
        model.addAttribute("authorName", authorName);
        model.addAttribute("searchWordDto", new SearchWordDto());
        return "books/author";
    }

    /**
     * Метод возвращает страницу с книгами по тегу
     * @param tagId
     * @param offset
     * @param limit
     * @param model
     * @return
     */
    @GetMapping("/tags/{tagId}")
    public String findByTagId(@PathVariable Integer tagId,
                              @RequestParam(required = false, defaultValue = "0") Integer offset,
                              @RequestParam(required = false, defaultValue = "10") Integer limit,
                              Model model) {
        logger.info("GET /tags/tagId returns book by tag: " + tagId);
        Page<BookDto> books = bookService.getBooksByTag(tagId, offset, limit);
        model.addAttribute("category", "Книги по тегу");
        model.addAttribute("booksByTag", books);
        SearchWordDto searchWordDto = new SearchWordDto();
        model.addAttribute("searchWordDto", searchWordDto);
        model.addAttribute("sourcePage", "tag");
        model.addAttribute("refreshId", tagId);
        model.addAttribute("showMoreButton", books.getContent().size() == limit);
        model.addAttribute("tagName", tagRepository.findById(tagId).isPresent() ?
                tagRepository.findById(tagId).get().getTagName() : "");
        return "tags/index";
    }

    /**
     * Метод подгружает следующую порцию книг на странице книг по тегу
     * @param tagId
     * @param offset
     * @param limit
     * @return
     */
    @GetMapping("/page/tags/{tagId}")
    public ResponseEntity<BooksPageDto> getPageBooksByTag(@PathVariable Integer tagId,
                                                          @RequestParam(required = false) Integer offset,
                                                          @RequestParam(required = false) Integer limit) {
        logger.info("GET page by tag books");
        Page<BookDto> books = bookService.getBooksByTag(tagId, offset, limit);
        return new ResponseEntity<>(
                new BooksPageDto(books.getContent()), HttpStatus.OK);
    }

    /**
     * Метод возвращает страницу с книгами по жанру
     * @param slug
     * @param offset
     * @param limit
     * @return
     */
    @GetMapping("/genre/{slug}")
    public ResponseEntity<BooksPageDto> getPageBooksByGenre(@PathVariable String slug,
                                                            @RequestParam(required = false, defaultValue = "0") Integer offset,
                                                            @RequestParam(required = false, defaultValue = "5") Integer limit) {
        logger.info("GET page of genres books");
        Page<BookDto> booksPage = genreService.getPageBooksByGenreSlug(slug, offset, limit);
        return new ResponseEntity<>(
                new BooksPageDto(booksPage.getContent()), HttpStatus.OK);
    }

    private LocalDate stringToLocalDate(String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        return LocalDate.parse(dateString, formatter);
    }

    private String localDateToString(LocalDate date) {
        return date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
    }
}
