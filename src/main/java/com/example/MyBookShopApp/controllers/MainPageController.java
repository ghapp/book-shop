package com.example.MyBookShopApp.controllers;

import com.example.MyBookShopApp.book.BookService;
import com.example.MyBookShopApp.dto.BooksPageDto;
import com.example.MyBookShopApp.book.tag.TagCount;
import com.example.MyBookShopApp.dto.SearchWordDto;
import com.example.MyBookShopApp.dto.BookDto;
import lombok.RequiredArgsConstructor;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class MainPageController {
    private final Logger logger = Logger.getLogger(MainPageController.class);

    private final BookService bookService;

    @ModelAttribute("searchWordDto")
    public SearchWordDto searchWordDto() {
        return new SearchWordDto();
    }

    // Список "Рекомендуемое" на главной странице
    @ModelAttribute("recommendedBooks")
    public List<BookDto> recommendedBooks() {
        return bookService.getPageOfRecommendedBooksDto(0, 10).getBooks();
    }

    // Список "Новинки" на главной странице
    @ModelAttribute("recentBooks")
    public List<BookDto> recentBooks() {
        return bookService.getPageOfRecentBooksDto(0, 10);
    }

    // Список "Популярное" на главной странице
    @ModelAttribute("popularBooks")
    public List<BookDto> popularBooks() {
        return bookService.getBooksDtoSortedByAverageRating(0, 10);
    }

    @ModelAttribute("searchResults")
    public List<BookDto> searchResults() {
        return new ArrayList<>();
    }

    @ModelAttribute("mainPageTags")
    public List<TagCount> mainPageTags() {
        return bookService.getMainPageTags();
    }

    @GetMapping("/")
    public String mainPage() {
        logger.info("GET main page returns index");
        return "index";
    }

    @GetMapping("/postponed")
    public String postponed() {
        logger.info("GET /postponed returns postponed books");
        return "postponed";
    }
    @GetMapping("/cart")
    public String cart() {
        logger.info("GET /cart returns cart");
        return "cart";
    }

    @GetMapping("/signin")
    public String signin() {
        logger.info("GET /signin returns signin");
        return "signin";
    }

    @GetMapping(value = {"/search", "/search/{searchWord}"})
    public String getSearchResults(@PathVariable(value = "searchWord", required = false) String searchWord,
                                   Model model) {
        logger.info("GET /search returns search, searchWord: " + searchWord);

        // Проверка на длину слова поиска
        if(searchWord == null || searchWord.length() < 3) {
            return "redirect:/";
        }

        SearchWordDto searchWordDto = new SearchWordDto();
        searchWordDto.setExample(searchWord);
        model.addAttribute("searchWordDto", searchWordDto);
        model.addAttribute("sourcePage", "search");
        model.addAttribute("searchResults",
                bookService.getPageDtoOfSearchResult(searchWordDto.getExample(), 0, 5));
        return "search/index";
    }

    @GetMapping("/search/page/{searchWord}")
    @ResponseBody
    public BooksPageDto getNextSearchPage(@RequestParam Integer offset,
                                          @RequestParam Integer limit,
                                          @PathVariable(value = "searchWord", required = false) String searchWord,
                                          Model model) {
        SearchWordDto searchWordDto = new SearchWordDto();
        searchWordDto.setExample(searchWord);
        model.addAttribute("sourcePage", "search");
        return new BooksPageDto(
                bookService.getPageDtoOfSearchResult(searchWordDto.getExample(), offset, limit));
    }

    @GetMapping("/documents")
    public String documents() {
        logger.info("GET /documents returns documents");
        return "documents/index";
    }

    @GetMapping("/about")
    public String about() {
        logger.info("GET /about returns about");
        return "about";
    }

    @GetMapping("/faq")
    public String faq() {
        logger.info("GET /faq returns faq");
        return "faq";
    }

    @GetMapping("/contacts")
    public String contacts() {
        logger.info("GET /contacts returns contacts");
        return "contacts";
    }
}
