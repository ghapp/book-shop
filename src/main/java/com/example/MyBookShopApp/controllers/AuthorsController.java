package com.example.MyBookShopApp.controllers;

import com.example.MyBookShopApp.author.AuthorService;
import com.example.MyBookShopApp.dto.SearchWordDto;
import com.example.MyBookShopApp.dto.AuthorDto;
import com.example.MyBookShopApp.dto.BookDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
@Api(tags = "Authors")
public class AuthorsController {
    private final Logger logger = Logger.getLogger(AuthorsController.class);
    private final AuthorService authorService;

    @Autowired
    public AuthorsController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @ModelAttribute("authorsMap")
    public Map<String,List<AuthorDto>> authorsMap(){
        return authorService.getAuthorsMap();
    }

    @GetMapping("/authors")
    public String authorsPage(Model model){
        logger.info("GET /authors returns authors books");
        model.addAttribute("searchWordDto", new SearchWordDto());
        return "authors/index";
    }

    // Страница автора (адрес страницы — /authors/SLUG)
    @GetMapping("/authors/{slug}")
    public String authorPage(@PathVariable String slug, Model model){
        logger.info("GET /authors returns authors books");
        List<BookDto> books = authorService.getBookListByAuthorSlug(slug);
        model.addAttribute("books", books);
        model.addAttribute("searchWordDto", new SearchWordDto());
        Optional<AuthorDto> authorOptional = authorService.getAuthorBySlug(slug);
        authorOptional.ifPresent(authorDto -> model.addAttribute("author", authorDto));
        return "authors/slug";
    }

    @ApiOperation("method to get map of authors")
    @GetMapping("/api/authors")
    @ResponseBody
    public Map<String, List<AuthorDto>> authors(){
        return authorService.getAuthorsMap();
    }
}
