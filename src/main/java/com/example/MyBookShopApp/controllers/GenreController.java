package com.example.MyBookShopApp.controllers;

import com.example.MyBookShopApp.dto.SearchWordDto;
import com.example.MyBookShopApp.dto.BookDto;
import com.example.MyBookShopApp.genre.GenreDTO;
import com.example.MyBookShopApp.genre.GenreService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;

@Controller

public class GenreController {
    @Autowired
    private GenreService genreService;

    private final Logger logger = Logger.getLogger(GenreController.class);

    // все жанры
    @GetMapping("/genres")
    public String showGenres(Model model) {
        List<GenreDTO> genreDTOS = genreService.getAllGenresWithAmountForFrontend();
        model.addAttribute("genreDTOS", genreDTOS);
        model.addAttribute("searchWordDto", new SearchWordDto());
        return "genres/index";
    }

    // жанр по slug
    @GetMapping("/genres/{slug}")
    public String showGenresBySlug(@PathVariable String slug,
                                   @RequestParam(required = false, defaultValue = "0") Integer offset,
                                   @RequestParam(required = false, defaultValue = "5") Integer limit,
                                   Model model) {
        logger.info("GET /genres/SLUG returns genres by slug: " + slug);
        List<BookDto> books = genreService.getListBooksByGenreSlug(slug);
        model.addAttribute("books", books.stream().limit(limit).collect(Collectors.toList()));
        model.addAttribute("genre", genreService.getGenreNameBySlug(slug));
        model.addAttribute("searchWordDto", new SearchWordDto());
        model.addAttribute("sourcePage", "genre");
        model.addAttribute("refreshId", slug);

        model.addAttribute("showMoreButton", books.size() >= limit);
        return "genres/slug";
    }


}
