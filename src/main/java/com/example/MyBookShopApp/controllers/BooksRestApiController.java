package com.example.MyBookShopApp.controllers;

import com.example.MyBookShopApp.book.BookService;
import com.example.MyBookShopApp.dto.BooksPageDto;
import com.example.MyBookShopApp.dto.BookDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/books/")
@Api(tags = "Books ")
public class BooksRestApiController {
    private final Logger logger = Logger.getLogger(BooksRestApiController.class);
    private final BookService bookService;

    @Autowired
    public BooksRestApiController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("by-author")
    @ApiOperation("operation to get book list of bookshop by passed author name")
    public ResponseEntity<List<BookDto>> booksByAuthor(@RequestParam("author") String authorName) {
        logger.info("GET /api/books/by-author returns books by by-author: " + authorName);
        return ResponseEntity.ok(bookService.getBooksByAuthor(authorName));
    }

    @GetMapping("by-title")
    @ApiOperation("get book list by title")
    public ResponseEntity<List<BookDto>> booksByTitle(@RequestParam("title") String title) {
        logger.info("GET /api/books/by-title returns books by by-title: " + title);
        return ResponseEntity.ok(bookService.getBooksByTitle(title));
    }

    @GetMapping("/by-price-range")
    @ApiOperation("get books by price range from min price to max price")
    public ResponseEntity<List<BookDto>> priceRangeBooks(@RequestParam("min") Integer min, @RequestParam("max") Integer max) {
        logger.info("GET /api/books/by-price-range returns book by by-price-range: " + min + " - " + max);
        return ResponseEntity.ok(bookService.getBooksWithPriceBetween(min, max));
    }

    @GetMapping("/by-price")
    @ApiOperation("get books by price")
    public ResponseEntity<List<BookDto>> priceRangeBooks(@RequestParam("price") Integer price) {
        logger.info("GET /api/books/by-price returns book by by-price: " + price);
        return ResponseEntity.ok(bookService.getBooksWithPrice(price));
    }


    @GetMapping("with-max-price")
    @ApiOperation("get books by max price")
    public ResponseEntity<List<BookDto>> maxPriceBooks() {
        logger.info("GET /api/books/with-max-price returns book with-max-price");
        return ResponseEntity.ok(bookService.getBooksWithMaxPrice());
    }

    @GetMapping("bestsellers")
    @ApiOperation("get bestsellers books (best)")
    public ResponseEntity<List<BookDto>> bestsellersBooks() {
        logger.info("GET /api/books/bestsellers returns bestsellers (is_bestseller = 1)");
        return ResponseEntity.ok(bookService.getBestsellers());
    }

    @GetMapping("/recommended")
    @ApiOperation("get recommended books")
    @ResponseBody
    public BooksPageDto getRecommendedBooksPage(@RequestParam Integer offset, @RequestParam Integer limit) {
        logger.info("GET /books/recommended returns recommended books");
        return new BooksPageDto(
                bookService.getPageOfRecommendedBooksDto(offset, limit).getBooks());
    }

    @GetMapping("/recent")
    @ApiOperation("get page recent books")
    @ResponseBody
    public BooksPageDto getRecentBooksPage(@RequestParam Integer offset, @RequestParam Integer limit) {
        logger.info("GET /api/books/recent returns recent books");
        return new BooksPageDto(
                bookService.getPageOfRecentBooksDto(offset, limit));
    }

    @GetMapping("/popular")
    @ApiOperation("get popular books")
    @ResponseBody
    public BooksPageDto getPopularBooksPage(@RequestParam Integer offset, @RequestParam Integer limit) {
        logger.info("GET popular returns popular books");
        return new BooksPageDto(
                bookService.getBooksDtoSortedByAverageRating(offset, limit));
    }
}
