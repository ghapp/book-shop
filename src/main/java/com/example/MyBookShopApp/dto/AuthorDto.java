package com.example.MyBookShopApp.dto;

import com.example.MyBookShopApp.author.Author;
import com.example.MyBookShopApp.book.Book;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthorDto {
    private Integer id;
    private String photo;
    /** slug - мнемонический идентификатор автора, который будет отображаться в ссылке на его страницу */
    private String slug;
    private String name;
    private String description;
    private List<Book> bookList = new ArrayList<>();

    public AuthorDto(Author author){
        this.id = author.getId();
        this.photo = author.getPhoto();
        this.slug = author.getSlug();
        this.name = author.getName();
        this.description = author.getDescription();
        this.bookList = author.getBookList();
    }
}
