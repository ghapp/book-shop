package com.example.MyBookShopApp.dto;


import com.example.MyBookShopApp.author.Author;
import com.example.MyBookShopApp.book.Book;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookDto {
    private int id;
    private LocalDate pubDate;
    private Integer isBestseller;
    private String slug;
    private String title;
    private String image;
    private Author author;
    private String description;
    private Integer priceOld;
    private Integer price;

    public BookDto(Book book) {
        this.id = book.getId();
        this.pubDate = book.getPubDate();
        this.isBestseller = book.getIsBestseller();
        this.slug = book.getSlug();
        this.title = book.getTitle();
        this.image = book.getImage();
        this.author = book.getAuthor();
        this.description = book.getDescription();
        this.priceOld = book.getPriceOld();
        this.price = book.getPrice();
    }
}
