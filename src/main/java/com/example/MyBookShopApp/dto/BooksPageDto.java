package com.example.MyBookShopApp.dto;

import com.example.MyBookShopApp.dto.BookDto;
import lombok.Data;

import java.util.List;

@Data
public class BooksPageDto {
    private Integer count;
    private List<BookDto> books;
    public BooksPageDto(List<BookDto> books) {
        this.books = books;
        this.count = books.size();
    }
}
