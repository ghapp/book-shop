package com.example.MyBookShopApp.book.links;

import com.example.MyBookShopApp.book.Book;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "ratings")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Rating {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne()
    @JoinColumn(name = "book_id", referencedColumnName = "id")
    private Book book;

    private int grade;
}
