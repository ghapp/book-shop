package com.example.MyBookShopApp.book.links;

import com.example.MyBookShopApp.book.Book;
import com.example.MyBookShopApp.genre.GenreEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "book2genre")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Book2GenreEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name="book_id", referencedColumnName = "id")
    private Book book;

    @ManyToOne
    @JoinColumn(name="genre_id", referencedColumnName = "id")
    private GenreEntity genre;

    @Override
    public String toString() {
        return "Book2GenreEntity{" +
                "id=" + id + '}';
    }
}
