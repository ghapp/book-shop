package com.example.MyBookShopApp.book;

import com.example.MyBookShopApp.book.tag.Tag;
import com.example.MyBookShopApp.book.tag.TagCount;
import com.example.MyBookShopApp.dto.BookDto;
import com.example.MyBookShopApp.dto.BooksPageDto;
import com.example.MyBookShopApp.repo.BookRepository;
import com.example.MyBookShopApp.repo.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookService {

    private final BookRepository bookRepository;
    private final TagRepository tagRepository;

    @Autowired
    public BookService(BookRepository bookRepository, TagRepository tagRepository) {
        this.bookRepository = bookRepository;
        this.tagRepository = tagRepository;
    }

    public BookDto getBookBySlug(String slug) {
        return bookRepository.getBookBySlug(slug).map(BookDto::new).orElse(null);
    }

    public List<BookDto> getBooksByAuthor(String authorName) {
        return bookRepository.findBooksByAuthorNameContaining(authorName).stream()
                .map(BookDto::new)
                .collect(Collectors.toList());
    }

    public List<BookDto> getBooksByTitle(String title) {
        return bookRepository.findBooksByTitleContaining(title).stream()
                .map(BookDto::new).collect(Collectors.toList());
    }

    public List<BookDto> getBooksWithPriceBetween(Integer min, Integer max) {
        return bookRepository.findBooksByPriceBetween(min, max).stream()
                .map(BookDto::new).collect(Collectors.toList());
    }

    public List<BookDto> getBooksWithPrice(Integer price) {
        return bookRepository.findBooksByPriceIs(price).stream()
                .map(BookDto::new).collect(Collectors.toList());
    }

    public List<BookDto> getBooksWithMaxPrice() {
        return bookRepository.getBooksWithMaxDiscount().stream()
                .map(BookDto::new).collect(Collectors.toList());
    }

    public List<BookDto> getBestsellers() {
        return bookRepository.getBestsellers().stream()
                .map(BookDto::new).collect(Collectors.toList());
    }

    // todo: реализовать после реализации алгоритма популярных книг, а это не ранее чем появится личный кабинет юзера
    public BooksPageDto getPageOfRecommendedBooksDto(Integer offset, Integer limit) {
        return new BooksPageDto(
                getBooksSortedByAverageRating(offset, limit).stream().map(BookDto::new)
                        .collect(Collectors.toList()));
    }

    public Page<BookDto> getPageOfRecentBooksDtoInDateRange(LocalDate from, LocalDate to, Integer offset, Integer limit) {
        Pageable nextPage = PageRequest.of(offset, limit);
        return bookRepository.findBookByPubDateBetweenOrderByPubDateDesc(from, to, nextPage)
                .map(BookDto::new);
    }

    public List<BookDto> getPageOfRecentBooksDto(Integer offset, Integer limit) {
        Pageable nextPage = PageRequest.of(offset, limit);
        return bookRepository.findAllByOrderByPubDateDesc(nextPage).getContent().stream().map(
                BookDto::new).collect(Collectors.toList());
    }

    public List<BookDto> getPageDtoOfSearchResult(String searchWord, Integer offset, Integer limit) {
        Pageable nextPage = PageRequest.of(offset, limit);
        Page<Book> books = bookRepository.findBookByTitleContainingIgnoreCase(searchWord, nextPage);
        return books.stream().map(BookDto::new).collect(Collectors.toList());
    }

    public List<Book> getBooksSortedByAverageRating(Integer offset, Integer limit) {
        Pageable nextPage = PageRequest.of(offset, limit);
        Page<Object[]> booksAndRatings = bookRepository.findAllSortedByAverageRating(nextPage);
        return booksAndRatings.getContent().stream().map(
                objects -> (Book) objects[0]).collect(Collectors.toList());
    }

    public List<BookDto> getBooksDtoSortedByAverageRating(Integer offset, Integer limit) {
        Pageable nextPage = PageRequest.of(offset, limit);
        Page<Object[]> booksAndRatings = bookRepository.findAllSortedByAverageRating(nextPage);
        List<Book> entities = booksAndRatings.getContent().stream().map(
                objects -> (Book) objects[0]).collect(Collectors.toList());
        return entities.stream().map(BookDto::new).collect(Collectors.toList());
    }

    public List<TagCount> getMainPageTags() {
        List<Object[]> results = tagRepository.findTagCounts();
        List<TagCount> tagCounts = new ArrayList<>();
        long maxCount = tagRepository.findMaxBooksCountPerTag();
        for (Object[] result : results) {
            Tag tag = (Tag) result[0];
            Long count = (Long) result[1];
            TagCount tagCount = new TagCount();
            tagCount.setTagId(tag.getId());
            tagCount.setTagName(tag.getTagName());
            tagCount.setCount(count.intValue());
            tagCount.setWeight((double) count / maxCount);
            tagCounts.add(tagCount);
        }

        return tagCounts;
    }

    public Page<BookDto> getBooksByTag(Integer tagId, Integer offset, Integer limit) {
        Pageable nextPage = PageRequest.of(offset, limit);
        return bookRepository.findByTagId(tagId, nextPage).map(BookDto::new);
    }
}
