package com.example.MyBookShopApp.book.file;

import javax.persistence.*;

@Entity
@Table(name = "book_file")
public class BookFileEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /** случайный хэш, предназначенный для идентификации файла при скачивании. */
    @Column(columnDefinition = "VARCHAR(255) NOT NULL")
    private String hash;

    @OneToOne

    private BookFileTypeEntity typeId;

    @Column(columnDefinition = "VARCHAR(255) NOT NULL")
    private String path;





}
