package com.example.MyBookShopApp.book.tag;

import lombok.Data;

@Data
public class TagCount {
    private Integer tagId;
    private String tagName;
    private Integer count;
    private Double weight;

    public String getTagSize() {
        if (weight <= 0.2) {
            return "Tag Tag_xs";
        } else if (weight > 0.2 && weight <= 0.4) {
            return "Tag Tag_sm";
        } else if (weight > 0.4 && weight <= 0.6) {
            return "Tag";
        } else if (weight > 0.6 && weight <= 0.8) {
            return "Tag Tag_md";
        } else {
            return "Tag Tag_lg";
        }
    }
}


