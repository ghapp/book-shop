package com.example.MyBookShopApp.repo;

import com.example.MyBookShopApp.book.tag.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TagRepository extends JpaRepository<Tag, Integer> {
    @Query("SELECT t.id, COUNT(b.id) FROM Tag t JOIN t.books b GROUP BY t.id")
    List<Object[]> calculateTagsCount();

    @Query(value = "SELECT MAX(count) FROM (SELECT COUNT(book_id) as count FROM book_tags GROUP BY tag_id) as subquery",
            nativeQuery = true)
    Long findMaxBooksCountPerTag();

    @Query("SELECT t, COUNT(b.id) as count FROM Tag t JOIN t.books b GROUP BY t")
    List<Object[]> findTagCounts();


}
