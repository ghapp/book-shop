package com.example.MyBookShopApp.repo;

import com.example.MyBookShopApp.book.links.Book2GenreEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface Book2genreRepository extends JpaRepository<Book2GenreEntity, Long> {
    Long countByGenreId(int genreId);
    List<Book2GenreEntity> findByBookId(int bookId);
    List<Book2GenreEntity> findByGenreId(int genreId);
    Page<Book2GenreEntity> findByGenreId(int genreId, Pageable pageable);
//    Page<Book2GenreEntity> findAllById(List<Integer> ids, Pageable pageable);

    @Query("SELECT b FROM Book2GenreEntity b WHERE b.genre.id IN :ids")
    Page<Book2GenreEntity> findAllByIds(List<Integer> ids, Pageable pageable);

}
