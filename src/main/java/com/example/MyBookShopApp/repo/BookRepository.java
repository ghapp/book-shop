package com.example.MyBookShopApp.repo;

import com.example.MyBookShopApp.book.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface BookRepository extends JpaRepository<Book, Integer> {
    Optional<Book> getBookBySlug(String slug);

    @Query("SELECT b FROM Book b ORDER BY b.pubDate DESC")
    List<Book> findAllOrderByPubDateDesc();

    Page<Book> findAllByOrderByPubDateDesc(Pageable nextPage);


    Page<Book> findBookByPubDateBetweenOrderByPubDateDesc(LocalDate from, LocalDate to, Pageable nextPage);

    @Query("SELECT b, COALESCE(AVG(r.grade), 0) as avgRating FROM Book b LEFT JOIN b.ratings r GROUP BY b ORDER BY avgRating DESC")
    Page<Object[]> findAllSortedByAverageRating(Pageable pageable);


    List<Book> findBooksByAuthorNameContaining(String authorFirstName);

    List<Book> findBooksByTitleContaining(String bookTitle);

    List<Book> findBooksByPriceBetween(Integer min, Integer max);

    List<Book> findBooksByPriceIs(Integer price);

    @Query("from Book  where isBestseller=1")
    List<Book> getBestsellers();

    @Query(value = "SELECT * FROM books WHERE discount = (SELECT MAX(discount) FROM books)", nativeQuery = true)
    List<Book> getBooksWithMaxDiscount();

    Page<Book> findBookByTitleContainingIgnoreCase(String bookTitle, Pageable nextPage);

    @Query(value = "SELECT b FROM Book b JOIN b.tags t WHERE t.id = :tagId",
            countQuery = "SELECT count(b) FROM Book b JOIN b.tags t WHERE t.id = :tagId")
    Page<Book> findByTagId(@Param("tagId") Integer tagId, Pageable pageable);
}
