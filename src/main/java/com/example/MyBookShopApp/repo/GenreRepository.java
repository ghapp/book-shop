package com.example.MyBookShopApp.repo;

import com.example.MyBookShopApp.genre.GenreEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GenreRepository extends JpaRepository<GenreEntity, Long> {
    Optional<GenreEntity> findByName(String name);
    Optional<GenreEntity> findBySlug(String slug);
}
