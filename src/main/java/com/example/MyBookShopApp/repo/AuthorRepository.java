package com.example.MyBookShopApp.repo;

import com.example.MyBookShopApp.author.Author;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthorRepository extends JpaRepository<Author, Integer> {
    Optional<Author> findAuthorBySlug(String authorSlug);
}
