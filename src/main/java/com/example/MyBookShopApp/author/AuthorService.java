package com.example.MyBookShopApp.author;

import com.example.MyBookShopApp.book.Book;
import com.example.MyBookShopApp.dto.AuthorDto;
import com.example.MyBookShopApp.dto.BookDto;
import com.example.MyBookShopApp.repo.AuthorRepository;
import com.example.MyBookShopApp.repo.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AuthorService {
    private final AuthorRepository authorRepository;

    @Autowired
    private AuthorService(
            AuthorRepository authorRepository
    ) {
        this.authorRepository = authorRepository;
    }

    public Map<String, List<AuthorDto>> getAuthorsMap() {
        List<AuthorDto> authorEntities = authorRepository.findAll().stream()
                .map(AuthorDto::new)
                .collect(Collectors.toList());
        return authorEntities.stream().collect(Collectors.groupingBy(
                a -> a.getName().substring(0, 1)
        ));
    }

    public List<BookDto> getBookListByAuthorSlug(String authorSlug) {
        Author author = authorRepository.findAuthorBySlug(authorSlug).orElseThrow(
                () -> new EntityNotFoundException("Author not found"));
        return author.getBookList().stream().map(BookDto::new)
                .collect(Collectors.toList());
    }

    public Optional<AuthorDto> getAuthorBySlug(String slug) {
        return authorRepository.findAuthorBySlug(slug).map(AuthorDto::new);
    }
}
